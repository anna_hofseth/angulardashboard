# AngularDashboard

##About
This is Dashboard, a website with 5 different APIs implemented to simulate a dashboard-type website

##APIs:
- MetaWeather: https://www.metaweather.com/
- Nasa: https://api.nasa.gov/
- EvilInsult: https://evilinsult.com/
- Age of Empires II: https://age-of-empires-2-api.herokuapp.com
- TheCatAPI: https://thecatapi.com/docs.html

##Setup development
- Clone repo
- Have node installed
- cd to repo
- Run command:
`npm install`

##Development
`ng serve`

##Production
`ng build`