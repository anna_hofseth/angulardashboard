import { Component, OnInit } from '@angular/core';
import { InsultService } from './../../services/insult.service';
import 'rxjs';

@Component({
  selector: 'app-insult',
  templateUrl: './insult.component.html',
  styleUrls: ['./insult.component.scss'],
  providers: [InsultService]
})
export class InsultComponent implements OnInit
{
  public insult: string;

  constructor(protected insultService: InsultService) { }

  ngOnInit()
  {
    this.insultService.getInsult()
    .subscribe(
      ins => {
        this.insult = ins.insult;
    });
  }
}
