import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsultComponent } from './insult.component';

describe('InsultComponent', () => {
  let component: InsultComponent;
  let fixture: ComponentFixture<InsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
