import { Component, OnInit } from '@angular/core';
import { CatsService } from './../../services/cats.service';
import 'rxjs';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss'],
  providers: [CatsService]
})

export class CatsComponent implements OnInit
{
  public catUrl: string;

  constructor(protected catsService: CatsService) { }

  ngOnInit()
  {
    this.catsService.getCat()
      .subscribe(
        cat => {
          this.catUrl = cat[0].url;
    });
  }
}
