import { Component, OnInit } from '@angular/core';
import { Aoe2Service } from './../../services/aoe2.service';
import 'rxjs';

@Component({
  selector: 'app-aoe2',
  templateUrl: './aoe2.component.html',
  styleUrls: ['./aoe2.component.scss'],
  providers: [Aoe2Service]
})

export class Aoe2Component implements OnInit
{
  public civilizations: any[];

  constructor(protected aoe2Service: Aoe2Service) { }

  ngOnInit()
  {
    this.aoe2Service.getSivilizations()
    .subscribe(
      civs => {
        this.civilizations = civs.civilizations;
    });
  }
}
