import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Aoe2Component } from './aoe2.component';

describe('Aoe2Component', () => {
  let component: Aoe2Component;
  let fixture: ComponentFixture<Aoe2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Aoe2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Aoe2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
