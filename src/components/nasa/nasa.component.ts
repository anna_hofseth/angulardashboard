import { Component, OnInit } from '@angular/core';
import { NasaService } from './../../services/nasa.service';
import 'rxjs';

@Component({
  selector: 'app-nasa',
  templateUrl: './nasa.component.html',
  styleUrls: ['./nasa.component.scss'],
  providers: [NasaService]
})

export class NasaComponent implements OnInit
{
  public title: string;
  public mediaUrl: string;

  public isVideo: boolean;

  constructor(protected nasaService: NasaService) { }

  ngOnInit()
  {
    this.nasaService.getSpaceObject()
      .subscribe(
        spacy => {
          this.title = spacy.title;
          this.mediaUrl = spacy.url;

          switch(spacy.media_type)
          {
            case "video":
              this.isVideo = true; break;
            
            default: this.isVideo = false;
          }
    });
  }

  getMediaUrl()
  {
    return this.mediaUrl;
  }
}