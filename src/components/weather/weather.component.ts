import { Component, OnInit } from '@angular/core';
import { WeatherService } from './../../services/weather-service.service';
import 'rxjs';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  providers: [WeatherService]
})

export class WeatherComponent implements OnInit
{
  public weatherTitle: String;
  public weatherDetail;

  public tempNow: String;

  constructor(protected weatherService: WeatherService) { }

  ngOnInit()
  {
    this.weatherService.getWeekForecast()
    .subscribe(
      weather => {
        this.weatherTitle = weather.title + ", " + weather.parent.title;
        this.weatherDetail = weather.consolidated_weather;
    });
  }

  round(num: number)
  {
    return Math.round(num);
  }
}
