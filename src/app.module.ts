import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app/app.component';
import { LoginComponent } from './components/login-component/login-component.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { routing } from './app.routes';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { WeatherComponent } from './components/weather/weather.component';
import { NasaComponent } from './components/nasa/nasa.component';
import { InsultComponent } from './components/insult/insult.component';
import { CatsComponent } from './components/cats/cats.component';
import { Aoe2Component } from './components/aoe2/aoe2.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardPageComponent,
    LoginPageComponent,
    WeatherComponent,
    NasaComponent,
    InsultComponent,
    CatsComponent,
    Aoe2Component,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    routing,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
