import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CatsService
{
  private url: string = 'https://api.thecatapi.com/v1/images/search';
  private key: string = "0a838b23-4715-4b4a-8ac8-5437ea1add7e";

  constructor(private http: Http) { }
  
  getCat(): Observable<any>
  {
    return this.http.get(this.url).pipe(
      map(response => {
        return response.json();
      })
    );
  }
}
