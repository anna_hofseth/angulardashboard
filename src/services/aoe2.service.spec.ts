import { TestBed, inject } from '@angular/core/testing';

import { Aoe2Service } from './aoe2.service';

describe('Aoe2Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Aoe2Service]
    });
  });

  it('should be created', inject([Aoe2Service], (service: Aoe2Service) => {
    expect(service).toBeTruthy();
  }));
});
