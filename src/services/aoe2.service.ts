import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class Aoe2Service
{
  private sivUrl: string = "https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations";
  private conversionUrl = 'https://cors-anywhere.herokuapp.com/';

  constructor(private http: Http) { }

  getSivilizations(): Observable<any>
  {
    return this.http.get(this.conversionUrl + this.sivUrl).pipe(
      map(response => {
        return response.json();
      })
    );
  }
}
