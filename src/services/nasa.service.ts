import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class NasaService
{
  private key: string = "?api_key=8oXSTCWMFYj1QwlINsZM0hHf6n10LXx8ORl1hWNR";
  private url: string = "https://api.nasa.gov/planetary/apod";
  private conversionUrl = 'https://cors-anywhere.herokuapp.com/';

  constructor(private http: Http) { }

  getSpaceObject(): Observable<any>
  {
    return this.http.get(this.conversionUrl + this.url + this.key).pipe(
      map(response => {
        return response.json();
      })
    );
  }
}
