import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})

export class DashboardPageComponent implements OnInit
{
  public catCount: Number = 10;
  
  constructor(protected router: Router) {
   }

  ngOnInit()
  {
    if (!sessionStorage.getItem('AuthToken')) {
      this.router.navigate(['/']);
    }
  }
}
